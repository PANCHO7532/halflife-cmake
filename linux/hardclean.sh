#!/bin/bash
# Execute at your own risk.
# This cleans every CMake generated file and object from project folders
# ...so you can do an clean CMake rebuild afterwards.
rm -rf game
rm -rf CMakeCache.txt && rm -rf CMakeFiles && rm cmake_install.cmake && rm Makefile && rm *.cbp && rm *.layout
rm -rf hl_cdll/CMakeCache.txt && rm -rf hl_cdll/CMakeFiles && rm hl_cdll/cmake_install.cmake && rm hl_cdll/Makefile && rm hl_cdll/*.cbp && rm hl_cdll/*.layout
rm -rf hldll/CMakeCache.txt && rm -rf hldll/CMakeFiles && rm hldll/cmake_install.cmake && rm hldll/Makefile && rm hldll/*.cbp && rm hldll/*.layout
rm -rf dmc_cdll/CMakeCache.txt && rm -rf dmc_cdll/CMakeFiles && rm dmc_cdll/cmake_install.cmake && rm dmc_cdll/Makefile && rm dmc_cdll/*.cbp && rm dmc_cdll/*.layout
rm -rf dmcdll/CMakeCache.txt && rm -rf dmcdll/CMakeFiles && rm dmcdll/cmake_install.cmake && rm dmcdll/Makefile && rm dmcdll/*.cbp && rm dmcdll/*.layout
rm -rf ricochet_cdll/CMakeCache.txt && rm -rf ricochet_cdll/CMakeFiles && rm ricochet_cdll/cmake_install.cmake && rm ricochet_cdll/Makefile && rm ricochet_cdll/*.cbp && rm ricochet_cdll/*.layout
rm -rf ricochetdll/CMakeCache.txt && rm -rf ricochetdll/CMakeFiles && rm ricochetdll/cmake_install.cmake && rm ricochetdll/Makefile && rm ricochetdll/*.cbp && rm ricochetdll/*.layout
# Cleanup of utils
rm -rf bspinfo/CMakeCache.txt && rm -rf bspinfo/CMakeFiles && rm bspinfo/cmake_install.cmake && rm bspinfo/Makefile
rm -rf light/CMakeCache.txt && rm -rf light/CMakeFiles && rm light/cmake_install.cmake && rm light/Makefile
rm -rf makefont/CMakeCache.txt && rm -rf makefont/CMakeFiles && rm makefont/cmake_install.cmake && rm makefont/Makefile
rm -rf makels/CMakeCache.txt && rm -rf makels/CMakeFiles && rm makels/cmake_install.cmake && rm makels/Makefile
rm -rf mdlviewer/CMakeCache.txt && rm -rf mdlviewer/CMakeFiles && rm mdlviewer/cmake_install.cmake && rm mdlviewer/Makefile
rm -rf mkmovie/CMakeCache.txt && rm -rf mkmovie/CMakeFiles && rm mkmovie/cmake_install.cmake && rm mkmovie/Makefile
rm -rf procinfo/CMakeCache.txt && rm -rf procinfo/CMakeFiles && rm procinfo/cmake_install.cmake && rm procinfo/Makefile
rm -rf qbsp2/CMakeCache.txt && rm -rf qbsp2/CMakeFiles && rm qbsp2/cmake_install.cmake && rm qbsp2/Makefile
rm -rf qcsg/CMakeCache.txt && rm -rf qcsg/CMakeFiles && rm qcsg/cmake_install.cmake && rm qcsg/Makefile
rm -rf qlumpy/CMakeCache.txt && rm -rf qlumpy/CMakeFiles && rm qlumpy/cmake_install.cmake && rm qlumpy/Makefile
rm -rf qrad/CMakeCache.txt && rm -rf qrad/CMakeFiles && rm qrad/cmake_install.cmake && rm qrad/Makefile
rm -rf serverctrl/CMakeCache.txt && rm -rf serverctrl/CMakeFiles && rm serverctrl/cmake_install.cmake && rm serverctrl/Makefile
rm -rf smdlexp/CMakeCache.txt && rm -rf smdlexp/CMakeFiles && rm smdlexp/cmake_install.cmake && rm smdlexp/Makefile
rm -rf sprgen/CMakeCache.txt && rm -rf sprgen/CMakeFiles && rm sprgen/cmake_install.cmake && rm sprgen/Makefile
rm -rf studiomdl/CMakeCache.txt && rm -rf studiomdl/CMakeFiles && rm studiomdl/cmake_install.cmake && rm studiomdl/Makefile
rm -rf vis/CMakeCache.txt && rm -rf vis/CMakeFiles && rm vis/cmake_install.cmake && rm vis/Makefile
rm -rf xwad/CMakeCache.txt && rm -rf xwad/CMakeFiles && rm xwad/cmake_install.cmake && rm xwad/Makefile
exit